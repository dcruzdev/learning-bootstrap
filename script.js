$(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $(".carousel").carousel({interval: 5000});
        $("#staticBackdrop").on("show.bs.modal", function (e){
          console.log("el modal se esta mostrando")
          $(e.relatedTarget).removeClass("btn-primary")
          $(e.relatedTarget).addClass("btn-outline-success")
          $(e.relatedTarget).prop("disable", true)
          $(e.relatedTarget).attr("data-modal", true)
        });
        $("#staticBackdrop").on("shown.bs.modal", function (e){
          console.log("el modal se mostró")
        });
        $("#staticBackdrop").on("hidde.bs.modal", function (e){
          console.log("el modal se oculta")
        });
        $("#staticBackdrop").on("hidden.bs.modal", function (e){
          console.log("el modal se ocultó")
          console.log(e)
          $("[data-modal='true']").prop("disable", false)
          $("[data-modal='true']").removeClass("btn-outline-success")
          $("[data-modal='true']").addClass("btn-success")
          $(".btn-reservar").removeAttr("data-modal")
        });
      })